import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 19.04.2015.
 */
public class ModelChangedDispatcher {

    private List<ModelChangedListener> listeners;

    public ModelChangedDispatcher(){
        listeners = new ArrayList<ModelChangedListener>();
    }

    public void dispatchEvent(ModelChangedEvent e){
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).modelChanged(e);
    }

    public void addEventListener(ModelChangedListener listener){
        listeners.add(listener);
    }

    public void removeEventListener(ModelChangedListener listener){
        listeners.remove(listener);
    }
}
