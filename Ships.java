import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 20.04.2015.
 */
public class Ships {

    private List<Ship> ships;

    private int oneCount;
    private int twoCount;
    private int threeCount;
    private int fourCount;


    public Ships(){
        ships = new ArrayList<Ship>();
    }

    public boolean allShipsLocated(){
        return oneCount == 4 && twoCount == 3 && threeCount == 2 && fourCount == 1;
    }

    public List<Ship> getShipsList(){
        return ships;
    }

    public boolean addShip(Ship ship){
        boolean shipAdded = false;
        switch (ship.getShipSize()){
            case ONE:
                if(oneCount < 4) {
                    ships.add(ship);
                    oneCount++;
                    shipAdded  = true;
                }
                break;

            case TWO:
                if(twoCount < 3) {
                    ships.add(ship);
                    shipAdded = true;
                    twoCount++;
                }
                break;

            case THREE:
                if(threeCount < 2){
                    ships.add(ship);
                    shipAdded = true;
                    threeCount++;
                }
                break;

            case FOUR:
                if(fourCount < 1){
                    ships.add(ship);
                    shipAdded = true;
                    fourCount++;
                }
                break;

            default:
                throw new IllegalArgumentException("Unexpected sheep size\n");
        }

        return shipAdded;
    }
}
