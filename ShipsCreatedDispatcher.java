import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 20.04.2015.
 */
public class ShipsCreatedDispatcher {
    private List<ShipsCreatedListener> listeners;

    public ShipsCreatedDispatcher(){
        listeners = new ArrayList<ShipsCreatedListener>();
    }

    public void dispatchEvent(ShipsCreatedEvent e){
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).shipsCreated(e);
    }

    public void addEventListener(ShipsCreatedListener listener){
        listeners.add(listener);
    }

    public void removeEventListener(ShipsCreatedListener listener){
        listeners.remove(listener);
    }
}
