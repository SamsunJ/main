import java.util.EventListener;

/**
 * Created by Администратор on 19.04.2015.
 */
public interface FieldCellClickedListener extends EventListener{
    public void fieldCellClicked(FieldCellClickedEvent e);
}
