import javax.swing.*;

/**
 * Created by Администратор on 20.04.2015.
 */
public class Game extends JComponent {

    private GameModel model;
    private GameView view;
    private GameController controller;

    public Game(Ships firstPlayerShips, Ships secondPlayerShips){
        model = new GameModel(firstPlayerShips, secondPlayerShips);
        controller = new GameController(model, new AI());
        view = new GameView(model, controller);
        add(view);
    }
}
