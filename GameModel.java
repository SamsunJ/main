import java.util.List;

/**
 * Created by Администратор on 19.04.2015.
 */
public class GameModel {

    FIELD_VALUE[][] playerField;
    private int playersShipCellsCount;

    FIELD_VALUE[][] aiField;
    private int aiShipCellsCount;

    public static final int FIELD_WIDTH = 10;
    public static final int FIELD_HEIGHT = 10;

    private ModelChangedDispatcher eventDispatcher;

    public boolean isPlayersTurn;

    public static enum FIELD_VALUE{
        EMPTY_FIELD,
        SHIP,
        SHOOTED_SHIP,
        MISS
    }

    public GameModel(Ships playerShips, Ships aiShips){
        isPlayersTurn = true;
        initFields(playerShips, aiShips);
        setEventDispatcher();
    }

    public int getPlayerShipCellsCount(){
        return playersShipCellsCount;
    }

    public int getAIShipCellsCount(){
        return aiShipCellsCount;
    }

    public void setPlayerFieldValue(int i, int j, FIELD_VALUE value){
        playerField[i][j] = value;
        if(value == FIELD_VALUE.SHOOTED_SHIP)
            playersShipCellsCount--;

        eventDispatcher.dispatchEvent(new ModelChangedEvent(this, i, j));
    }

    public FIELD_VALUE[][] getPlayerField(){
        return playerField;
    }

    public void setAIFieldValue(int i, int j, FIELD_VALUE value){
        aiField[i][j] = value;
        if(value == FIELD_VALUE.SHOOTED_SHIP)
            aiShipCellsCount--;

        eventDispatcher.dispatchEvent(new ModelChangedEvent(this, i, j));
    }

    public FIELD_VALUE[][] getAIField(){
        return aiField;
    }

    public void addEventListener(ModelChangedListener listener){
        eventDispatcher.addEventListener(listener);
    }

    public void removeEventListener(ModelChangedListener listener){
        eventDispatcher.removeEventListener(listener);
    }

    private void initFields(Ships playerShips, Ships aiShips){
        playerField = getField(playerShips);
        playersShipCellsCount = getShipCellsCount(playerShips);
        aiField = getField(aiShips);
        aiShipCellsCount = getShipCellsCount(aiShips);
    }

    public int getShipCellsCount(Ships ships){
        int shipCellsCount = 0;
        List<Ship> shipList = ships.getShipsList();
        for(int i = 0; i < shipList.size(); i++){
            switch (shipList.get(i).getShipSize()){
                case ONE:
                    shipCellsCount += 1;
                    break;

                case TWO:
                    shipCellsCount += 2;
                    break;

                case THREE:
                    shipCellsCount += 3;
                    break;

                case FOUR:
                    shipCellsCount += 4;
                    break;
            }
        }

        return shipCellsCount;
    }

    private FIELD_VALUE[][] getField(Ships ships){
        FIELD_VALUE[][] fieldValues = new FIELD_VALUE[FIELD_WIDTH][FIELD_HEIGHT];
        for(int i = 0; i < FIELD_HEIGHT; i++)
            for(int j = 0; j < FIELD_WIDTH; j++)
                fieldValues[i][j] = FIELD_VALUE.EMPTY_FIELD;

        List<Ship> shipList = ships.getShipsList();
        for(int i = 0; i < shipList.size(); i++){
            Ship currentShip = shipList.get(i);
            int width = currentShip.getWidth();
            int height = currentShip.getHeight();
            for(int row = 0; row < height; row++){
                for(int col = 0; col < width; col++){
                    fieldValues[row + currentShip.getStartRow()][col + currentShip.getStartColumn()] = FIELD_VALUE.SHIP;
                }
            }
        }

        return fieldValues;
    }

    private void setEventDispatcher(){
        eventDispatcher = new ModelChangedDispatcher();
    }
}
