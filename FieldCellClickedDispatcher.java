import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 19.04.2015.
 */
public class FieldCellClickedDispatcher {

    private List<FieldCellClickedListener> listeners;

    public FieldCellClickedDispatcher(){
        this.listeners = new ArrayList<FieldCellClickedListener>();
    }

    public void dispatchEvent(FieldCellClickedEvent event){
        for(int i = 0; i < listeners.size(); i++)
            listeners.get(i).fieldCellClicked(event);
    }

    public void addEventListener(FieldCellClickedListener listener){
        listeners.add(listener);
    }

    public void removeEventListener(FieldCellClickedListener listener){
        listeners.remove(listener);
    }
}

