import java.util.EventListener;

/**
 * Created by Администратор on 19.04.2015.
 */
public interface ModelChangedListener extends EventListener{
    public void modelChanged(ModelChangedEvent e);
}
