/**
 * Created by Администратор on 19.04.2015.
 */
public class GameController implements FieldCellClickedListener{
    private GameModel model;

    private AI currentAI;

    public GameController(GameModel model, AI currentAI){
        this.model = model;
        this.currentAI = currentAI;
    }

    public void fieldCellClicked(FieldCellClickedEvent e){
        if(model.isPlayersTurn)
            parsePlayerShoot(e.getRow(), e.getCol());
    }

    private void parsePlayerShoot(int i, int j){
        GameModel.FIELD_VALUE nextState = getCellNextState(model.getAIField()[i][j]);
        if (nextState != null) {
            model.setAIFieldValue(i, j, nextState);
            if (nextState == GameModel.FIELD_VALUE.MISS) {
                model.isPlayersTurn = false;
                aiTurn();
            }
        }
    }

    private void aiTurn(){
        while(model.isPlayersTurn == false){
            Cell aiMadeChoice = currentAI.makeChoice();
            parseAIShoot(aiMadeChoice.row, aiMadeChoice.col);
        }
    }

    private void parseAIShoot(int i, int j){
        GameModel.FIELD_VALUE nextState = getCellNextState(model.getPlayerField()[i][j]);
        if (nextState != null) {
            model.setPlayerFieldValue(i, j, nextState);
            if (nextState == GameModel.FIELD_VALUE.MISS) {
                model.isPlayersTurn = true;
            }
        }
    }

    private GameModel.FIELD_VALUE getCellNextState(GameModel.FIELD_VALUE currentFieldValue){
        switch (currentFieldValue){
            case EMPTY_FIELD:
                return GameModel.FIELD_VALUE.MISS;

            case SHIP :
                return GameModel.FIELD_VALUE.SHOOTED_SHIP;

            //уже стреляли сюда
            case SHOOTED_SHIP:
            case MISS:
                return null;

            default:
                throw new IllegalArgumentException("Unexpected FIELD_VALUE");
        }
    }
}
