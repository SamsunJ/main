import javax.swing.*;

/**
 * Created by Администратор on 20.04.2015.
 */
public class MainApplet extends JApplet{
    private static GameModel model;
    private static GameView view;
    private static GameController controller;

    private static AI ai;

    private static CreateShipsPanel createShipsPanel;

    public void init(){
        createShipsPanel = new CreateShipsPanel();
        add(createShipsPanel);

        createShipsPanel.addEventListener(new ShipsCreatedListener() {
            @Override
            public void shipsCreated(ShipsCreatedEvent e) {
                createGame(e.getShips());
            }
        });

    }

    private void createGame(Ships playerShips){
        ai = new AI();
        remove(createShipsPanel);
        Ships aiShips = ai.generateShips();
        model = new GameModel(playerShips, aiShips);
        controller = new GameController(model, ai);
        view = new GameView(model, controller);
        add(view);
    }
}
