import java.util.EventObject;

/**
 * Created by Администратор on 20.04.2015.
 */
public class ShipsCreatedEvent extends EventObject{

    private Ships ships;

    public ShipsCreatedEvent(Object source, Ships ships){
        super(source);
        this.ships = ships;
    }

    public Ships getShips(){
        return ships;
    }
}
