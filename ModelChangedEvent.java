import java.awt.*;
import java.util.EventObject;

/**
 * Created by Администратор on 19.04.2015.
 */
public class ModelChangedEvent extends EventObject{
    private int i;
    private int j;

    public ModelChangedEvent(Object source, int i, int j){
        super(source);
        this.i = i;
        this.j = j;
    }

    public int getRowNumber(){
        return i;
    }

    public int getColumnNumber(){
        return j;
    }
}
