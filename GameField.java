import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Администратор on 19.04.2015.
 */
public class GameField extends JPanel{

    private NotEditableTable field;

    private FieldCellClickedDispatcher dispatcher;

    private String emptyCell;
    private String shipCell;
    private String missCell;
    private String shootedCell;

    private JLabel fieldLabel;

    public GameField(String fieldTitle, GameModel.FIELD_VALUE[][] values, String emptyCell, String shipCell, String missCell,
                     String shootedCell){

        setCellsValueToDraw(emptyCell, shipCell, missCell, shootedCell);
        initField(fieldTitle, values);
        setDispatcher();
        field.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                parseMouseClicked(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                //
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                //
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //
            }
        });
    }

    private void setCellsValueToDraw(String emptyCell, String shipCell, String missCell, String shootedCell){
        this.emptyCell = emptyCell;
        this.shipCell = shipCell;
        this.missCell = missCell;
        this.shootedCell = shootedCell;
    }

    private void setDispatcher(){
        dispatcher = new FieldCellClickedDispatcher();
    }

    public void addFieldCellClickedListener(FieldCellClickedListener listener){
        dispatcher.addEventListener(listener);
    }

    public void deleteFieldCellClickedListener(FieldCellClickedListener listener){
        dispatcher.removeEventListener(listener);
    }

    private void parseMouseClicked(MouseEvent e){
        int row = field.rowAtPoint(e.getPoint());
        int col = field.columnAtPoint(e.getPoint());
        if(row != -1 && col != -1){
            dispatcher.dispatchEvent(new FieldCellClickedEvent(this, row, col));
        }
    }

    public void setFieldState(int i, int j, GameModel.FIELD_VALUE fieldValue){
        Object valueToDraw = getValueToDraw(fieldValue);
        field.setValueAt(valueToDraw, i, j);
    }

    private Object getValueToDraw(GameModel.FIELD_VALUE field_value){
        switch (field_value){
            case EMPTY_FIELD:
                return emptyCell;

            case MISS:
                return missCell;

            case SHIP:
                return shipCell;

            case SHOOTED_SHIP:
                return shootedCell;

            default:
                throw new IllegalArgumentException("Unexpected FIELD_VALUE");
        }
    }

    private NotEditableTable getField(GameModel.FIELD_VALUE[][] values){
        DefaultTableModel fieldModel = new DefaultTableModel(values.length, values[0].length);
        for(int i = 0; i < values.length; i++)
            for(int j = 0; j < values[i].length; j++){
                fieldModel.setValueAt(getValueToDraw(values[i][j]), i, j);
            }

        NotEditableTable field = new NotEditableTable(fieldModel, 40, 40);
        return field;
    }

    private void initField(String fieldTitle, GameModel.FIELD_VALUE[][] values){
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        fieldLabel = new JLabel(fieldTitle);
        add(fieldLabel);
        fieldLabel.setAlignmentX(CENTER_ALIGNMENT);

        field = getField(values);

        field.setAlignmentX(CENTER_ALIGNMENT);
        field.setAlignmentY(CENTER_ALIGNMENT);
        add(field);
    }
}
