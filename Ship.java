import java.util.IllegalFormatConversionException;

/**
 * Created by Администратор on 20.04.2015.
 */
public class Ship {

    public enum SHIP_ROTATION{
        ZERO,
        NINETY
    }

    public enum SHIP_SIZE{
        ONE,
        TWO,
        THREE,
        FOUR
    }

    private int i;
    private int j;
    private int sizeInt;

    private SHIP_ROTATION rotation;
    private SHIP_SIZE size;


    public Ship(SHIP_ROTATION rotation, SHIP_SIZE size, int i, int j){
        this.i = i;
        this.j = j;
        this.rotation = rotation;
        this.size = size;
        this.sizeInt = convertToInt(size);
    }

    private int convertToInt(SHIP_SIZE size){
        int shipSize;

        switch (size){
            case ONE:
                shipSize = 1;
                break;

            case TWO:
                shipSize = 2;
                break;

            case THREE:
                shipSize = 3;
                break;

            case FOUR:
                shipSize = 4;
                break;
            default:
                throw new IllegalArgumentException("UNEXPECTED SHIP_SIZE");
        }

        return shipSize;
    }

    public int getWidth(){
        return 1 + ((rotation == SHIP_ROTATION.ZERO ? 1 : 0) * (sizeInt - 1));
    }

    public int getHeight(){
        return 1 + ((rotation == SHIP_ROTATION.ZERO ? 0 : 1) * (sizeInt - 1));
    }

    public boolean isValid(){
        boolean isValidFlag = false;
        int height = getHeight();
        int width = getWidth();
        if((i >= 0 && i + height <= GameModel.FIELD_HEIGHT) && (j >= 0 && j + width <= GameModel.FIELD_WIDTH))
            isValidFlag = true;

        return isValidFlag;
    }

    public int getShipSizeInt(){
        return sizeInt;
    }

    public int getStartRow(){
        return i;
    }

    public int getStartColumn(){
        return j;
    }

    public SHIP_ROTATION getShipRotation(){
        return rotation;
    }

    public SHIP_SIZE getShipSize(){
        return size;
    }
}
