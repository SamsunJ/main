import java.util.EventObject;

/**
 * Created by Администратор on 19.04.2015.
 */
public class FieldCellClickedEvent extends EventObject{

    private int row;
    private int col;

    public FieldCellClickedEvent(Object source, int row, int col){
        super(source);
        this.row = row;
        this.col = col;
    }

    public int getRow(){
        return row;
    }

    public int getCol(){
        return col;
    }
}
