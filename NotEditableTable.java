import javax.swing.*;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * Created by Администратор on 19.04.2015.
 */
public class NotEditableTable extends JTable{

    public NotEditableTable(TableModel tableModel, int cellWidth, int cellHeight){
        super(tableModel);
        setCellSelectionEnabled(false);
        setFocusable(false);
        TableColumnModel model = getColumnModel();
        for(int i = 0; i < model.getColumnCount(); i++){
            model.getColumn(i).setPreferredWidth(cellWidth);
        }

        this.setRowHeight(cellHeight);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
